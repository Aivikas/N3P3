﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hommik
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"..\..\opilased.txt";
            string[] readText = File.ReadAllLines(path);
            foreach (string s in readText)
            {
                Console.WriteLine(s);
            }

            File.ReadAllLines(path).ToList().ForEach(s => Console.WriteLine(s));
            
        }
    }
}